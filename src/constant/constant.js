export const COLORS = {
    primary: '#09090f',
    secondary: '#51535e',
    ptext : '#ffffff',
    stext : '#a0a0a0',
    tabinactive : '#51535d'
}
export const FONTS = {
    light: 'Poppins-Light',
    medium : 'Poppins-Medium',
    semibold : 'Poppins-SemiBold',
}

export const SIZES = {
    sm : 10,
    md : 12,
    lg: 14,
    xl : 18,
    xxl : 24,
    xxxl : 36,
}

export const PADDING = {
    ph : 48,
    pv : 54,
    tab : 56,
    nav : 24
}